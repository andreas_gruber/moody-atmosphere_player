var Application = {
/* 
 * youtube api key
 */  
    youtubeApiKey: 'AIzaSyDEOXC6DevZD76ewhJgDNxEolupM3HHo7s',
    
 /* 
 * set database
 */   
    database: new Firebase('https://moody-atmosphere.firebaseio.com'),

/* 
 * user login
 */
	user: null, 
/* 
 * youtube video duration storage
 */
	duration: null, 
	
/* 
 * youtube player storage
 */
	player: null,
	
/* 
 * playlist
 */
	playlist: [],
	
/* 
 * playlist html storage
 */
	playlistHTML: null,
	
/* 
 * track
 */	
	trackCount: 0,
	
	init: function() {
	    gapi.client.setApiKey(Application.youtubeApiKey);
	    setTimeout(function() {
	        gapi.client.load('youtube', 'v3', function() {
    	        start = function() {
            	    if(typeof YT == "undefined") {
            	        start();
            	    }else{
            	        if(location.hash == '') {
            	        playlistId = 'PL2z_F9i1q62QpT2X5M3M_eJmVPeoIteKK'
                	    }else{
                	         playlistId = location.hash.replace('#','');
                	    }
                	    Application.loadPlaylists();
                	    Application.loadPlayer();
                        Application.setEvents();
                	    //$('#loading').modal('hide');
            	    }
            	}
            	start();
	        });
	    },1);
	},
	
	loadPlaylists:function() {
	    
	    var request = gapi.client.youtube.playlists.list({
                channelId: 'UCvOyritx8R2bn5g9S3Uf8EA',
                part: 'id,snippet',
                maxResults: 50
        });
            
        request.execute(function(response) {
            var channel = response
            var source   = $("#playlist-item-template").html();
            var template = Handlebars.compile(source);
            for(c=0;c < channel.items.length; c++) {
                playlist = channel.items[c];
                var listTitle = playlist.snippet.title.split(',');
                var playlistId =  playlist.id;
                var thumbnail = playlist.snippet.thumbnails.high.url;
                var title = listTitle[0];
                var genre = '#'+listTitle[1];
                var mood = '#'+listTitle[2];
                var context = {playlistId: playlistId, thumbnail:thumbnail,title: title,mood:mood,genre:genre }
                var html    = template(context);
                $('#userPlaylists .row').append(html);
            }
        });
	},
    
    loadPlaylist: function(playlistId) {
        console.log(playlistId);
        $('#playlist ul li').remove();
         Application.trackCount = 0;
        
        var request = gapi.client.youtube.playlistItems.list({
                playlistId: playlistId,
                part: 'id,snippet',
                maxResults: 50
        });
        
        Application.playlist = [];
        
        request.execute(function(response) {
            var video = response
            for(c=0;c < video.items.length; c++) {
                title = video.items[c].snippet.title.replace(/\[[a-zA-Z0-9 -_]*\]( )(-*)(:*)/g,'').replace(/\[(.*?)\]/g,'').substring(0, 45) + "..."
                Application.playlist.push(video.items[c].snippet.resourceId.videoId);
                Application.setListItem(title,video.items[c].snippet.resourceId.videoId);
                length = video.items.length -1
                if(c == length) {
                    Application.setTime();
                    Application.startVideo();
                }
            }
        });
    },
    
/* 
 * set item in list
 */		
	setListItem: function(title,id) {
	    var source   = $("#list-item-template").html();
        var template = Handlebars.compile(source);
        var context = {id: id, title: title, key: Application.playlist.indexOf(id)}
        var html    = template(context);
        $('#playlist > ul').append(html);
	},
	
/* 
 * load youtube player
 */	
	loadPlayer: function() {
		Application.player = new YT.Player('player', {
				height: '390',
				width: '640',
				playerVars: { 'theme': 'light', 'modestbranding': 1, 'iv_load_policy' : 3,'controls': 0},
				events: {
				'onReady': Application.onPlayerReady,
				'onStateChange': Application.onPlayerStateChange
			}
		});
	},
	
/* 
 * start video when youtube loaded
 */		
	onPlayerReady: function(event) {
        //
	},

/* 
 * when player state changed start next video
 */	
	onPlayerStateChange: function(event) {
		if (event.data == YT.PlayerState.ENDED) {
            Application.startVideo(event);
        }
        if (event.data == YT.PlayerState.PLAYING) {
            Application.duration = Application.player.getDuration();
            $('.fullTime').text(Application.formatTime(Application.duration));
        }
        
        
	},

/* 
 * start next video and update count for next video id
 */		
	startVideo: function() {
        if(Application.trackCount < Application.playlist.length) {
            Application.player.cueVideoById(Application.playlist[Application.trackCount]);
            Application.player.playVideo();
            Application.setTitle();
            Application.setActive();
            Application.trackCount++;
        }
	},

/* 
 * different control events
 */	
	setEvents: function() {
	    
        $('#playlist ul').on('click','.track>a', function(event) {
            event.preventDefault();
            var data = $(this).parent().data(); 
            Application.trackCount = parseInt(data.key);
            Application.startVideo();
        });
        
        $('.controlbar .next').on('click', function() {
            Application.startVideo();
        });
        
        $('.controlbar .prev').on('click', function() {
            Application.trackCount = Application.trackCount-2;
            Application.startVideo();
        });
        
        $('.controlbar .play').on('click', function() {
            Application.player.playVideo();
        });
        
        $('.controlbar .pause').on('click', function() {
            Application.player.pauseVideo();
        });
        
        $('.controlbar .progressbar').on('click', function(e) {
            var posX = $(this).offset().left
            var seek = e.pageX - posX;
            fullTime = $(this).width();
            onePercent = parseInt(fullTime)/100;
            seekToPercent = parseInt(seek/onePercent);
            seekTo = parseInt(Application.duration/100*seekToPercent);
            Application.player.seekTo(seekTo);
        });
        
        $('#userPlaylists').on('click','.playlist', function(event) {
            event.preventDefault();
            var data = $(this).data();
            Application.loadPlaylist(data.playlistid);
            $('#playlist').animate({left:'0'},500)
            $('.controlbar').animate({left:'0'},500)
            $('#userPlaylists').animate({left:'25%',width: '70%'},500)
            
        })
	},
	
/* 
 * set current video title
 */		
	setTitle: function() {
        data = $('#'+Application.playlist[Application.trackCount]).data();
        $('.current').html(data.title)
	},
	
/* 
 * set current video time
 */		
	setTime: function() {
	    setInterval(function(){
            if(Application.duration != null) {
                currentTime = Application.player.getCurrentTime();
                $('.currentTime').text(Application.formatTime(currentTime));
                fullTime = Application.duration;
                onePercent = fullTime/100;
                currentPercent = currentTime/onePercent;
                $(".status").css('width',currentPercent+"%")
            }
        },100);
	},
	
/* 
 * set active video class
 */		
	setActive: function() {
        $('.active').removeClass('active');
        $('#'+Application.playlist[Application.trackCount]).addClass('active');
	},

/* 
 * format seconds to time
 */		
	formatTime: function(seconds) {
	    var seconds = Math.floor(seconds),
        hours = Math.floor(seconds / 3600);
        seconds -= hours*3600;
        var minutes = Math.floor(seconds / 60);
        seconds -= minutes*60;
    
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
	}
};

/* 
 * start script
 */	
$(document).ready(function() {
	console.log('Starting application...');
	$('#loading').modal('show');
});

googleApiClientReady = function() {
    
    Application.init(); 
    
    var auth = new FirebaseSimpleLogin(Application.database, function(error, user) {
        if (error) {
            // an error occurred while attempting login
            console.log(error);
        } else if (user) {
            // user authenticated with Firebase
            Application.user = user;
            console.log(Application.user)
            $('.user a').append('<img src="'+Application.user.thirdPartyUserData.picture+ '" width="24">');
            $('.user a').append('<span>'+Application.user.displayName+'</span>');
       
             $('.login').hide();
        } else {
        
        }
    });
    
    
    $('.login').click(function() {
        auth.login('google');
    })
    
}
